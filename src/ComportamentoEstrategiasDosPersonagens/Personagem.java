package ComportamentoEstrategiasDosPersonagens;

public abstract class Personagem {

	private String personagem;
	protected ISobreviver sobreviver;
	protected ILutar lutar;

	public Personagem(String personagem) {
		this.personagem = personagem;
	}

	public String getPersonagem() {
		return personagem;
	}

	public void sobreviver() {
		sobreviver.sobreviver();
	}

	public void setSobreviver(ISobreviver sobreviver) {
		this.sobreviver = sobreviver;
	}

	public void lutar() {
		lutar.lutar();
	}

	public void setLuta(ILutar lutar) {
		this.lutar = lutar;
	}

}
