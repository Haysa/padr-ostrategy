package ComportamentoEstrategiasDosPersonagens;

public class Escapar implements ISobreviver {

	@Override
	public void sobreviver() {
		System.out
				.println("Sobrevivendo entre trancos e barrancos "
						+ "durante a temporada escapando com a ajuda de amigos e inimigos que querem me matar!");

	}

}
