package ComportamentoEstrategiasDosPersonagens;

import Personagens.Arya;
import Personagens.DaenerysTargaryen;
import Personagens.JonSnow;
import Personagens.TheonGreyjoy;

public class LutaPorPortoReal {

	private Personagem[] personagem;

	public LutaPorPortoReal() {

		personagem = new Personagem[4];

		personagem[0] = new Arya("Arya Stark:");
		personagem[1] = new DaenerysTargaryen("Daenerys Targaryen:");
		personagem[2] = new JonSnow("Jon Snow:");
		personagem[3] = new TheonGreyjoy("Theon Greyjoy:");

	}

	/**
	 * Primeira temporada
	 */
	public void primeiraTemporada() {

		System.out.println("A saga come�a!!!");
		System.out.println("\n ##Primeira Temporada!##");
		for (int i = 0; i < 4; i++) {
			System.out.println(personagem[i].getPersonagem());
			personagem[i].lutar();

		}
		personagem[3].setLuta(new Matar());

	}

	/**
	 * Segunda Temporada
	 */
	public void segundaTemporada() {

		System.out.println("\n ##Segunda Temporada!##");
		for (int i = 0; i < personagem.length; i++) {
			System.out.println(personagem[i].getPersonagem());

			personagem[i].lutar();
		}
		personagem[1].setLuta(new UsandoDragoes());
		personagem[2].setSobreviver(new SeEsconder());
	}

	/**
	 * Terceira Temporada
	 */
	public void terceiraTemporada() {

		System.out.println("\n ##Terceira Temporada!##");
		for (int i = 0; i < personagem.length; i++) {
			System.out.println(personagem[i].getPersonagem());
			personagem[i].lutar();
			personagem[i].sobreviver();
		}
		personagem[3].setSobreviver(new MudancaDePersonalidade());

	}
}
