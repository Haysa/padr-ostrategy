package Personagens;

import ComportamentoEstrategiasDosPersonagens.Casar;
import ComportamentoEstrategiasDosPersonagens.GuerraComExercito;
import ComportamentoEstrategiasDosPersonagens.Matar;
import ComportamentoEstrategiasDosPersonagens.MudancaDePersonalidade;
import ComportamentoEstrategiasDosPersonagens.Personagem;

public class TheonGreyjoy extends Personagem {

	public TheonGreyjoy(String personagem) {
		super(personagem);
		super.sobreviver = new MudancaDePersonalidade();
		super.lutar = new Matar();

	}

}
