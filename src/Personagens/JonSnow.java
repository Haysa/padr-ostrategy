package Personagens;

import ComportamentoEstrategiasDosPersonagens.Escapar;
import ComportamentoEstrategiasDosPersonagens.Matar;
import ComportamentoEstrategiasDosPersonagens.Personagem;
import ComportamentoEstrategiasDosPersonagens.SeEsconder;

public class JonSnow extends Personagem {

	public JonSnow(String personagem) {
		super(personagem);

		super.lutar = new Matar();
		super.sobreviver = new SeEsconder();

	}

}
