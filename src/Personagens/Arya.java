package Personagens;

import ComportamentoEstrategiasDosPersonagens.Escapar;
import ComportamentoEstrategiasDosPersonagens.Matar;
import ComportamentoEstrategiasDosPersonagens.Personagem;
import ComportamentoEstrategiasDosPersonagens.Treinar;

public class Arya extends Personagem {

	public Arya(String personagem) {
		super(personagem);

		super.sobreviver = new Escapar();
		super.lutar = new Treinar();

	}

}
