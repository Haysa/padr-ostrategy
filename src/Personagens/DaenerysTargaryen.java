package Personagens;

import ComportamentoEstrategiasDosPersonagens.Casar;
import ComportamentoEstrategiasDosPersonagens.Personagem;
import ComportamentoEstrategiasDosPersonagens.UsandoDragoes;

public class DaenerysTargaryen extends Personagem {

	public DaenerysTargaryen(String personagem) {
		super(personagem);

		super.sobreviver = new Casar();
		super.lutar = new UsandoDragoes();

	}

}
